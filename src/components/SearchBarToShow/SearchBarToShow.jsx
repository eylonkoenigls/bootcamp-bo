import '../SearchBar/SearchBar.css'
import { useState } from "react";
import CreateNewPosition from '../createNewPosition/createNewPosition'
export default function SearchBar({ searchData, filterByShearch }) {
    const [showCreateNewPosition, setShowCreateNewPosition] = useState(false);
    return (
        <div className='search-bar-main z' >
            <div className='search-bar-container'>
                <div className="search-input-box">
                    <img className="searchIcon" src="Search@2x.svg" alt="search" />
                    <input

                        onChange={(event) => { filterByShearch(event.target.value) }}
                        type={"text"}
                        className="searchBox"
                        placeholder="חפש משרה, מועמד, תפקיד ועוד"
                    />
                </div>
            </div>
            {showCreateNewPosition && <CreateNewPosition />}
            <div className="btn-div">
                <button
                    onClick={() => {
                        showCreateNewPosition
                            ? setShowCreateNewPosition(false)
                            : setShowCreateNewPosition(true);
                        console.log("button details2 clicked");
                    }}
                    className="create-position-btn"><strong> צור משרה חדשה </strong></button>
            </div>
        </div>



    )


}