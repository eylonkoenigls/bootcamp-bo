import JobCard from "../JobCard/JobCard";
import "./AllJobCards.css";

export default function AllJobCards({ setAllJobs, filteredJobs, allJobs }) {


  return (
    <section className="AllJobCards-AllJobCards">
      {filteredJobs.map(({ job_description, title, _id, display_to_homePage }) => (
        <JobCard key={_id} setAllJobs={setAllJobs} allJobs={allJobs} company={job_description} title={title} id={_id} onHome={display_to_homePage} />
      ))}
    </section>
  );
}
