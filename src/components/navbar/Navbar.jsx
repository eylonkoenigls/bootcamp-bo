import React from "react";
import "./Navbar.css";
import { Link } from "react-router-dom";

function Navbar() {
  return (

    <div className="navbar-main-container ">
      <div className="navbar-routes-container bg" >
        <div className="elements-side-menu-tab-enabled" >
          <Link className="nav-links-labels" to="/OpenPositions">
            משרות פתוחות
          </Link>
        </div>
        <div className="elements-side-menu-tab-enabled" >
          <Link className="nav-links-labels" to="/FilledPositions">
            משרות תפוסות
          </Link>
        </div>
        <div className="elements-side-menu-tab-enabled">
          <Link className="nav-links-labels" to="/CandidateList">
            רשימת מועמדים
          </Link>
        </div>
        <div className="elements-side-menu-tab-enabled">
          <Link className="nav-links-labels" to="/PositionsOnHome">
            משרות על דף הבית
          </Link>
        </div>
      </div>
      {/* </nav> */}
    </div>

  );
}

export default Navbar;

//lalalal