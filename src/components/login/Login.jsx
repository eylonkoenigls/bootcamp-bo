import { useRef, useContext } from "react";
import "./Login.css";
import image from "./image.png";
import { Link } from "react-router-dom";
import AdminContext from "../../context/AdminContext";

export default function Login() {
  const inputName = useRef(null);
  const inputPass = useRef(null);
  const { handleLogin, inputsUpdate } = useContext(AdminContext)

  const validate = () => {
    localStorage.LSaccessToken !== "" ? handleLogin() : alert("you are not an admin");
  }

  return (
    <div className="container">
      <div className="people-img-container">
        <img id="img" src={image} alt="img" />
      </div>

      <div className="div2">

        <h1 className="header">ברוכים הבאים</h1>
        <h3 className="header2">הזינו פרטים לכניסה לאתר</h3>
        <form action="" onSubmit={(e) => handleLogin(e)} className="form-container">
          <div className="inputs">

            <input
              ref={inputName}
              type="text"
              name="username"
              placeholder="*שם משתמש"
              className="username"
              required

            />
            <input
              ref={inputPass}
              type="password"
              name="password"
              placeholder="*סיסמה "
              className="password"
              required
            />
          </div>
          <button type="submit" className="button-login" >
            <div className="elements-text-center">התחבר </div>
          </button>
          <div className="question">
            <div className="check-for-remember">
              <input className="login-chack-box" type="checkbox" />
              <div className="remember" >
                זכור אותי ממחשב זה
              </div>
            </div>
            <Link className="forget-password" to="/OpenPositions">שכחתי סיסמה</Link>
          </div>
          <h5 className="header3">או הירשם באמצעות</h5>
          <div className="btn-google">גוגל</div>

        </form>
      </div>
    </div>
  );
}
