import React, { useEffect, useState } from "react";
import "./OpenPositions.css";
import SearchBar from "../SearchBar/SearchBar";
import PopupCard from "../PopupCard/PopupCard";
import OpenPositionsTable from "../Tables/OpenPositionsTable/OpenPositionsTable";
import Navbar from "../navbar/Navbar";
import axios from "axios";
import api from "../../axiosReq";



function OpenPositions() {
  const [openPositionList, setOpenPositionList] = useState(null)

  // ].map((obj) => ({ ...obj, numberOfCandidates: obj.list_of_candidates.length }));

  const getOpenPositions = async () => {
    const res = await api.get("/editposition/manager/openPosition");
    const jobs = await res.data;
    setOpenPositionList(jobs);

    return res.data;
  }

  useEffect(() => {

    getOpenPositions()

  }, [])

  // localStorage.accessToken="eyJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI2MWVlYTJkNjI0YzMzOTFhZTM5Nzc1YjUiLCJ1c2VybmFtZSI6IlJhYmkgTWVsb2JhdmljaCIsInBhc3N3b3JkIjoiJDJiJDEwJHZvTS51MjlEczFqQ2c5UU8yaDU2ZE9qLzRxeUUyaDgzbW9oYVBWcU11M2dmd3hTQlNVcnZ1IiwiaXNBZG1pbiI6dHJ1ZSwiX192IjowfQ.a8CJnofmWTYdKpUeKaU4ygCdlJF7EjnqaatgpdvC1iw";
  // useEffect (() => {
  //    function get_all_open_jobs() {
  //     fetch(`http://3.16.162.99/api/openPosition/openPositionPage`, {
  //       method: "GET",
  //       headers: {
  //         "content-type": "application/json",
  //         authorization: `bearer ${JSON.parse(localStorage.accessToken)}`,
  //       }

  //     })
  //       .then((res) => res.json())
  //       .then((data) =>
  //         console.log(data)
  //       );

  //   }
  //   get_all_open_jobs();
  // },[])

  // data.map((position) => {
  //   const title = position.title;
  //   const years_of_experience = position.years_of_experience;
  //   const job_description = position.job_description;
  //   const job_should_start = position.job_should_start;
  //   const cvquntity = position.cvquntity;
  //   const changedBy = position.createdBy;
  //   const expired_date = position.expired_date;
  //   //האם מוסיפים בדף הראשי או רק בפופאפ
  //   // const contact = position.contact;
  //   // const phoneNumber = position.phoneNumber;
  //   // const address = position.address;
  // });

  // const [openPositionList, setOpenPositionList] = useState(initPositionList)
  // const [init, setInit] =useState(initPositionList)

  // cuseeffect>>fetch>>positionarray - fetch positions from server
  // try
  // const searchData = (x) => {
  //   console.log(x)
  // const query= init.filter((v) => v.title.includes(x))
  // // console.log(query);
  // setOpenPositionList(query)
  // }
  // useEffect(()=>{
  // console.log('set openPositionList',openPositionList);
  // },[openPositionList])
  // async function get_list_of_candidates() {
  //   const res = await fetch(`http://3.16.162.99/api/openPosition/candidates`, {
  //     method: "GET",
  //   });
  //   const data = await res.json();
  //   console.log(data);
  // }

  const get_all_manned_jobs = async () => {
    let res = await axios.get(
      `http://localhost:3000/api/openPosition/mannedPositionPage`
    );
    let data = await res.data;
    console.log(data);
  };
  get_all_manned_jobs();
  //list_of_candidates page


  const userAccessToken = `bearer eyJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI2MWVlYTJkNjI0YzMzOTFhZTM5Nzc1YjUiLCJ1c2VybmFtZSI6IlJhYmkgTWVsb2JhdmljaCIsInBhc3N3b3JkIjoiJDJiJDEwJHZvTS51MjlEczFqQ2c5UU8yaDU2ZE9qLzRxeUUyaDgzbW9oYVBWcU11M2dmd3hTQlNVcnZ1IiwiaXNBZG1pbiI6dHJ1ZSwiX192IjowfQ.a8CJnofmWTYdKpUeKaU4ygCdlJF7EjnqaatgpdvC1iw`;


  async function delete_position(id_position) {
    let res = await axios.put(
      `http://3.16.162.99/api/openPosition/position/deletePosition/${id_position}`,
      {
        headers: {
          "content-type": "application/json",
          authorization: `${userAccessToken}`,
        },
      }
    );
    const data = await res.data;
    console.log(data);
  }
  const id_position = "61f0351f46b27b553f3cc5a9";
  delete_position(id_position);

  async function editCandidate(candidate_data) {
    let res = await axios.put(
      `http://3.16.162.99/api/openPosition/editCandidate`,
      candidate_data,
      {
        headers: {
          "content-type": "application/json",
          authorization: `${userAccessToken}`,
        },
      }
    );
    const data = await res.data;
    console.log(data);
  }
  const candidate_data = {
    candidate: {
      rating: 0,
      name: "najkm",
      phoneNumber: "0521234501",
      yearsOfExperience: 3,
      fileURL:
        "https://bootcampbackoffice.s3.us-east-2.amazonaws.com/cv/1643130397491_sample.pdf",
      cvStatus: "accepted",
      statusChangedBy: "61eea2d624c3391ae39775b5",
    },
    user: "61eef17a431ba02321c9fda4",
    position: "61f035dc46b27b553f3cc5b9",
    submitedMySelf: false,
    createdAt: "2022-01-25T17:06:38.576Z",
    updatedAt: "2022-01-26T19:48:19.026Z",
  };
  editCandidate(candidate_data);

  return (
    <div className="app-window">
      <Navbar />
      <div className="main-window">
        <SearchBar />
        <div className="table-main">
          {openPositionList
            && openPositionList.length ?
            <OpenPositionsTable openPositionList1={openPositionList} /> :
            <div className="loading">loading...</div>
          }
        </div>
      </div>
    </div>
  );
}

// export const func_get_list_of_candidates

export default OpenPositions;