import React, { useEffect, useState } from "react";
import Navbar from "../navbar/Navbar";
import SearchBar from "../SearchBar/SearchBar";
import CandidateTable from "../Tables/CandidateTable/CandidateTable";
import "./CandidateList.css";

import axios from "axios";





// const candidateList =
// const candidateList = [
//   {
//     _id: "61ed92ae5699ee601e1a070f",
//     title: " בכיף מפצח תוכנה",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 2,
//     job_description: "איה הייתה פעם כיפה אדומה שחיפשה את סבתא שלה ומצאה רק עבודה רק בהייטק",
//     candidateName: "יוסי כהן",
//     phoneNumber: 34234 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 34234 - 32842,
//     residence: "הרצליה",
//     status: "בתהליך",
//     __v: 0,
//   },
//   {
//     title: "שלגייה",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 5,
//     job_description: "בלגייה חיפשה את הנסיך שלה בין הגמדים אך לא מצאה לצערה הרב",
//     candidateName: "מיכל כהן",
//     phoneNumber: 45145 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 21 / 12 / 2022,
//     residence: "גבעתיים",
//     status: "בתהליך",
//     __v: 0,
//   },
//   {
//     _id: "61ed93155699ee601e1a0713",
//     title: "היפייפיה הנרדמת",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 1,
//     job_description: "א…",
//     candidateName: "יונה כהן",
//     phoneNumber: 5725474 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 28 / 12 / 2022,
//     residence: "ירושלים",
//     status: "לא התקבל",
//   },
//   {
//     _id: "61ed92ae5699ee601e1a070f",
//     title: "מפצח תוכנה",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 9,
//     job_description: "היה הייתה פעם כיפה אדומה שחיפשה את סבתא שלה ומצאה רק עבודה רק בהייטק",
//     candidateName: "יורם כהן",
//     phoneNumber: 396072435087 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 40 / 12 / 2026,
//     residence: "אילת",
//     status: "התקבלה",
//     __v: 0,
//   },
//   {
//     _id: "61ed92e85699ee601e1a0711",
//     title: "שלגייה",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 12,
//     job_description: "שלגייה חיפשה את הנסיך שלה בין הגמדים אך לא מצאה לצערה הרב",
//     candidateName: "אביעד כהן",
//     phoneNumber: 5736473476 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 34234 - 32842,
//     residence: "גבעתיים",
//     status: "בתהליך",
//     __v: 0,
//   },
//   {
//     _id: "61ed93355699ee601e1a0715",
//     title: "המכשפה",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 2,
//     job_description: "אני מחפשת ילדים קטנים לנשנש תודה מראש",
//     candidateName: "נחמה כהן",
//     phoneNumber: 839604 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 34234 - 32842,
//     residence: "גבעת עדה",
//     status: "לא התקבלה",
//     __v: 0,
//   },
//   {
//     _id: "61ed93615699ee601e1a0717",
//     title: "דרדסים",
//     is_open: true,
//     display_to_homePage: false,
//     years_of_experience: 2,
//     job_description: "אם תהיה ממש בשקט תשמע אותנו... אז שששששששש",
//     candidateName: "דניאלה פיק",
//     phoneNumber: 673546357 - 32842,
//     statusChange: "עבסעשדע",
//     dateStart: 34234 - 32842,
//     residence: "בנימינה",
//     status: "בתהליך",
//     __v: 0,
//   },
// ]

function   CandidateList() {

  const [candidateList,setCandidateList]=useState([])
  // let candidateList2=[]
  const get_list_of_candidates = async () => {
    let res = await axios.get(`http://3.16.162.99/api/openPosition/candidates`);
    let data = await res.data;
    console.log(data)
    console.log("&&&&&&");
    setCandidateList(data)

    console.log(candidateList)

    //  candidateList2=data
    // return resData
  };
  
  useEffect(async()=>{
  
    await get_list_of_candidates()
    
    console.log("$$$$$$$$$$$$$");
//  console.log(candidat  eList);
},[])
// console.log(get_list_of_candidates());
// console.log("@@@@@@@");
// console.log( candidateList2);

  return <div className="app-window">
    <Navbar />
    <div className="main-window">
      <SearchBar />
      <div className="table-main">
        <CandidateTable candidateList={candidateList} />

      </div>
    </div>
  </div>
}

export default CandidateList;
