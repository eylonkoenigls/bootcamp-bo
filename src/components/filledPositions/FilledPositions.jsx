import React from "react";
import Navbar from "../navbar/Navbar";
import SearchBar from "../SearchBar/SearchBar";
import FilledPositionTable from "../Tables/FilledPositionsTable/FilledPositionsTable";
import api from "../../axiosReq";
import { useState, useEffect } from 'react';



function FilledPositions() {
  const [filledPositionList, setFilledPositionList] = useState(null)
  const fetchfilledPositionList = async () => {
    const res = await api.get("/openposition/mannedPositionPage");

    const jobs = res.data;
    console.log({jobs})
    setFilledPositionList(jobs);

    return res.data;
  }
  
  useEffect(() => {
  
    fetchfilledPositionList()
  
  }, [])
  return <div className="app-window">
    <Navbar />
    <div className="main-window">
      <SearchBar />
      <div className="table-main">
        {filledPositionList && filledPositionList.length &&

        <FilledPositionTable filledPositionList={filledPositionList} />
      }

      </div>
    </div>
  </div>
}

export default FilledPositions;
