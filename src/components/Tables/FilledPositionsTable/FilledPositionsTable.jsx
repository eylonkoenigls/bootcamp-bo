import '../Table.css'
import * as React from 'react';
import { useState, useEffect } from 'react';


const translate = {
  "שם מועמד": "candidateName",
  "שנות ניסיון": "years_of_experience",
  תפקיד: "title",
  "תחילת עבודה": "dateStart"
};




const initSortObject = {
  "שנות ניסיון": false,
  תפקיד: false,
  "שם מועמד": false,
  "תחילת עבודה": false,
};

export default function FilledPositionTable({filledPositionList}) {
  // const [sort, setSort] = useState(initSortObject);
  // const [positions, setPositions] = useState([...filledPositionList]);


  // function handleSort(ev) {
  //   const key = ev.target.value;
  //   const newState = { ...initSortObject };
  //   newState[key] = true;
  //   setSort(newState);
  // }

  // useEffect(() => {
  //   const sortOnField = Object.keys(sort).filter((k) => sort[k]);
  //   const a = Object.keys(sort);
  //   const b = a.find((k) => sort[k]);
  //   const translatedKey = translate[sortOnField];
  //   const sortedPositions = [...positions].sort((a, b) => (a[translatedKey] > b[translatedKey] ? 1 : b[translatedKey] > a[translatedKey] ? -1 : 0));
  //   if (translatedKey === "years_of_experience") {sortedPositions.reverse()};
  //   setPositions(sortedPositions);
  // }, [filledPositionList]);
  console.log({filledPositionList});

  function parseDate(date) {
      if(!date) return ''
      const paresingDate = new Date(date)
      return paresingDate.toLocaleDateString()
  }

const tableKeys = ["תפקיד", "תחילת עבודה", "שנות ניסיון", "תיאור משרה", "שם מועמד", "טלפון","שינוי סטטוס"]
  return (
    <div dir="rtl">
      <table className='table'  >
        <thead>
          <tr className='titles'>
            {tableKeys.map((val) =>
              (<th className='titles'>{val}  </th>))}
          </tr>
        </thead>
        <tbody >
          {filledPositionList.map((val) => {
              return (
                <tr className='hover'>
                  <td className='tableBoundries tit'>{val.position?.title}</td>
                  <td className='tableBoundries job_should_start'>{parseDate(val.position?.job_should_start)}</td>
                  <td className='tableBoundries years_of_experience'>{val.position?.years_of_experience}</td>
                  <td className='tableBoundries des'>{val.position?.job_description}</td>
                  <td className='tableBoundries tit'>{val.position?.candidateName}</td>
                  <td className='tableBoundries numberLine num'>{val.position?.phoneNumber}</td>
                  <td className='tableBoundries statusChangeBy'>{val.candidate?.statusChangedBy?.username}</td>
                </tr>
              )
            }
          )}
        </tbody>
      </table>
    </div>
  );
}