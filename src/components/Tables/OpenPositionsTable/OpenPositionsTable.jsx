import '../Table.css'
import * as React from 'react';
import { useState, useEffect } from 'react';
import PopupCard from '../../PopupCard/PopupCard';

const translate = {
  "כמות קו'ח": "numberOfCandidates",
  "שנות ניסיון": "years_of_experience",
  תפקיד: "title",
};


const initSortObject = {
  "שנות ניסיון": false,
  תפקיד: false,
  "כמות קו'ח": false,
};

export default function OpenPositionsTable({ openPositionList1 }) {
  console.log(openPositionList1);
  // const [sort, setSort] = useState(initSortObject);
  const [positions, setPositions] = useState([...openPositionList1]);
  const [show, setShow] = useState(false)

  // function handleSort(ev) {
  //   const key = ev.target.value;
  //   const newState = { ...initSortObject };
  //   newState[key] = true;
  //   setSort(newState);
  // }

  // useEffect(() => {
  //   const sortOnField = Object.keys(sort).filter((k) => sort[k]);
  //   const a = Object.keys(sort);
  //   const b = a.find((k) => sort[k]);
  //   const translatedKey = translate[sortOnField];
  //   const sortedPositions = [...positions].sort((a, b) => (a[translatedKey] > b[translatedKey] ? 1 : b[translatedKey] > a[translatedKey] ? -1 : 0));
  //   if ((translatedKey === "numberOfCandidates") || (translatedKey === "years_of_experience")) { console.log("good"); sortedPositions.reverse() };
  //   setPositions(sortedPositions);
  // }, [sort]);

  function searchData(x) {
    console.log(x)
    // const query = [...openPositionList].filter((v) => v.title.includes(x))
    // setPositions(query)

  }

  // useEffect(()=>{
  // console.log('set openPositionList',openPositionList);
  // },[openPositionList])


  // function searchData (ev.input.value) {


  // }

  const tableKeys = ["תפקיד", "שנות ניסיון", "תיאור משרה", "כמות קו'ח"]
  console.log(positions)
  return (
    <div dir="rtl">
      {show && <PopupCard setShow={setShow} />}
      <table className='table'  >
        <thead>
          <tr className='titles'>
            {tableKeys.map((val) =>
              (<th className='titles'  >{val} <button value={val} key={val} >^</button>  </th>))}
          </tr>
        </thead>
        <tbody >
          {positions.map((val) => {
            return (
              <tr className='hover' key={val._id}>
                <td className='tableBoundries tit' onDoubleClick={() => setShow(true)}>{val.title}</td>
                <td className='tableBoundries des' onDoubleClick={() => setShow(true)}>{val.years_of_experience}</td>
                <td className='tableBoundries des' onDoubleClick={() => setShow(true)}>{val.job_description}</td>
                <td className='tableBoundries numberLine num' onDoubleClick={() => setShow(true)} >{val.cvquntity}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}