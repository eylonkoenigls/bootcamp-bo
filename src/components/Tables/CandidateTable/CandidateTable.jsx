import '../Table.css'
import * as React from 'react';
import { useState, useEffect } from 'react';

const translate = {
  "שם מועמד": "candidateName",
  "טלפון": "phoneNumber",
  "שנות ניסיון": "years_of_experience",
  "מקום מגורים": "residence",
  תפקיד: "title",
  "סטטוס": "status"
};


const initSortObject = {
  "שנות ניסיון": false,
  תפקיד: false,
  "שם מועמד": false,
  "מקום מגורים": false,
  "טלפון": false,
  "סטטוס": false,
};

export default function CandidateTable({candidateList}) {
  const [sort, setSort] = useState(initSortObject);
  // console.log(candidateList);

  // const candidatesFormated=
  // const [candidates, setCandidates] = useState("ddddd");
  // const [candidates, setCandidates] = useState(candidateList);
  // const [candidates, setCandidates] = useState(candidateList);
  const candidates=candidateList

// console.log(candidates);
  function handleSort(ev) {
    const key = ev.target.value;
    const newState = { ...initSortObject };
    newState[key] = true;
    setSort(newState);
  }
const statusTranslate={
"in process": "בתהליך",
"rejected": "נדחה",
"accepted": "התקבל"
}

// in orsder to sort 
  // useEffect(() => {
  //   const sortOnField = Object.keys(sort).filter((k) => sort[k]);
  //   const a = Object.keys(sort);
  //   const b = a.find((k) => sort[k]);
  //   const translatedKey = translate[sortOnField];
  //   const sortedCandidates = [...candidates].sort((a, b) => (a[translatedKey] > b[translatedKey] ? 1 : b[translatedKey] > a[translatedKey] ? -1 : 0));
  //   if ((translatedKey === "years_of_experience")) {sortedCandidates.reverse()};
  //   setCandidates(sortedCandidates);
  // }, [sort]);

  const tableKeys = ["שם מועמד", "טלפון", "שנות ניסיון", "מקום מגורים", "תפקיד", "סטטוס"]
// console.log(candidates);
  return (
    <div dir="rtl">
      <table className='table'  >
        <thead>
          <tr className='titles'>
            {tableKeys.map((val) =>
              (<th className='titles'>{val} <button value={val} key={val} onClick={handleSort}>^</button>  </th>))}
          </tr>
        </thead>
        <tbody >
          {candidates.map((val) => {
            console.log(val);
            return (
              <tr className='hover'>
                <td className='tableBoundries tit'>{val.candidate.name}</td>
                <td className='tableBoundries numberLine num'>{val.candidate.phoneNumber}</td>
                <td className='tableBoundries numberLine num'>{val.candidate.yearsOfExperience}</td>
                <td className='tableBoundries tit'>ירושלים</td>
                <td className='tableBoundries tit'>{val.position?.title}</td>
                <td className='tableBoundries tit'>{statusTranslate[val.candidate.cvStatus]}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}