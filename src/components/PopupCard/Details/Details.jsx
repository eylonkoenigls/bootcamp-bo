import React from "react";
import "./Details.css";
export default function Details() {
  return (
    <div dir="rtl" className="details-text">
      <div className="details_component">
        <div className="space-between">
          <div id="details_space_between">טלפון</div>054-9961234
        </div>
        <div className="space-between">
          <div>שנות ניסיון</div>
          <div id="details_space_between">3</div>
        </div>
        <div className="space-between">
          <div>מקום מגורים</div>
          <div id="details_space_between">ארץ לעולם לא</div>
        </div>
        <div className="space-between">
          <div>תפקיד</div>
          <div id="details_space_between">פיראט</div>
        </div>
      </div>
    </div>
  );
}