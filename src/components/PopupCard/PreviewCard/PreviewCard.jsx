import "./PreviewCard.css";

export default function PreviewCard() {
  return (
    <div>
      <div className="previewCard">
        <div className="companyContainer">
          <div className="logoContainer">
            <div className="logo"></div>
          </div>
          <div className="companyLine"></div>
        </div>
        <div id="candidate_ditels">קיפי</div>
        <div id="candidate_ditels">דירוג</div>
        <div id="candidate_ditels">תאריך הגשה</div>
        <div className="statusContainer">
          <div className="statusUpdate">בתהליך</div>
          <div> :סטטוס </div>
        </div>
        <div class="line"></div>
      </div>
      <div className="previewCard">
        <div className="companyContainer">
          <div className="logoContainer">
            <div className="logo"></div>
          </div>
          <div className="companyLine"></div>
        </div>
        <div id="candidate_ditels">מויישה אופניק</div>
        <div id="candidate_ditels">דירוג</div>
        <div id="candidate_ditels">תאריך הגשה</div>
        <div className="statusContainer">
          <div className="statusUpdate">בתהליך</div>
          <div> :סטטוס </div>
        </div>
        <div class="line"></div>
      </div>
    </div>
  );
}