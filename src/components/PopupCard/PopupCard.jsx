import React from "react";
import "./PopupCard.css";
import { ReactComponent as ExitIcon } from "../../assets/exit@1.5x.svg";
import { ReactComponent as EditIcon } from "../../assets/edit@1.5x.svg";
import { ReactComponent as DeleteIcon } from "../../assets/delete@1.5x.svg";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useState } from "react";
import Details from "./Details/Details";
import PreviewCard from "./PreviewCard/PreviewCard";
export default function PopupCard({ setShow }) {
  const [showAppliedPositions, setShowAppliedPositions] = useState(false);
  const [showDetails, setShowDetails] = useState(false);

  return (
    <div className="main-card">
      <div className="side-card">
        <div>
          <div className="card-header">
            <div className="tittle_bar">
              <div className="title">תפקיד</div>
              <div className="button_bar">
                <button className="icon-button" onClick={() => setShow(false)}>
                  <ExitIcon />
                </button>
                <button
                  className="icon-button"
                  onClick={() => console.log("button edit clicked")}
                >
                  <EditIcon />
                </button>
                <button
                  className="icon-button"
                  onClick={() => console.log("button delete clicked")}
                >
                  <DeleteIcon />
                </button>
              </div>
            </div>
          </div>
          <div class="line"></div>
        </div>
        <div>
          <div className="details">
            <div className="title">פרטי משרה</div>
            <button
              className="open-down"
              onClick={() => {
                showDetails ? setShowDetails(false) : setShowDetails(true);
                console.log("button details clicked");
              }}
            >
              <ExpandMoreIcon />
            </button>
          </div>
          {showDetails && <Details />}
          <div class="line"></div>
        </div>
        <div>
          <div className="details" id="candidate-details">
            <div id="applied-positions" className="title">
              מועמדים
            </div>
            <button
              className="open-down applied-positions "
              onClick={() => {
                showAppliedPositions
                  ? setShowAppliedPositions(false)
                  : setShowAppliedPositions(true);
                console.log("button details2 clicked");
              }}
            >
              <ExpandMoreIcon />
            </button>
          </div>
          <div class="line"></div>
        </div>
        {showAppliedPositions && <PreviewCard />}
      </div>
    </div>
  );
}