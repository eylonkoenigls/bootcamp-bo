
import SearchBarToShow from "../SearchBarToShow/SearchBarToShow";
import { useState, useEffect } from "react";
import AllJobCards from "../AllJobCards/AllJobCards";
import Navbar from "../navbar/Navbar";
import api from "../../axiosReq";
import SendBar from "../SendBar/SendBar";

function PositionsOnHome() {
  const [allJobs, setAllJobs] = useState([]);
  const [filteredJobs, setFilteredJobs] = useState([""])

  const getPositions = async () => {
    const res = await api.get("/editposition/manager/openPosition");
    const jobs = await res.data.filter(job => job.active && job.is_open);
    setAllJobs(jobs)
    setFilteredJobs(jobs);
    return res.data;
  }


  useEffect(() => {
    getPositions()
  }, [])


  // useEffect(() => {
  //   setFilteredJobs(allJobs);
  // }, [allJobs])


  const filterByShearch = (type) => {

    console.log({ type });
    if (type !== '') {
      const filter = allJobs.filter((job) => job.title.includes(type))
      setFilteredJobs(filter)
    } else {
      setFilteredJobs(allJobs)
    }
  }
  // console.log(allJobs);

  // const [homeJobs, setHomeJobs] = useState(mockDataJobs.filter(onHome=true));


  return (
    <div className="app-window">
      <Navbar />
      <div className="main-window">
        <SearchBarToShow filterByShearch={filterByShearch} />

        <AllJobCards setAllJobs={setAllJobs} filteredJobs={filteredJobs} allJobs={allJobs} />
        {allJobs && allJobs.length &&
          <SendBar allJobs={allJobs} />
        }

      </div>
    </div>
  )
}

export default PositionsOnHome;
