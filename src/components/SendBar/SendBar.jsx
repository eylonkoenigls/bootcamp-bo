import './SendBar.css'
import api from "../../axiosReq";
export default function SendBar({ allJobs }) {



    const checkedJobs = allJobs.filter((v) => v.display_to_homePage === true)
    let color = checkedJobs.length !== 6 ? "red" : "green";


    const send = async () => {
        const body = { positions: checkedJobs };
        const res = await api.put("/editposition/manager/displayPositionToHomePage", body);
        alert("המידע נשלח בהצלחה 😊")
        const jobs = await res.data;

    }

    return (
        <div className='send-bar-main' >
            <div className='send-bar-container' style={{ color: color }} >
                <span className='span-num-of-choices'>נבחרו 6/{checkedJobs.length} משרות </span>
            </div>
            <div className="btn-div">
                <button className="create-position-btn" disabled={checkedJobs.length !== 6} onClick={() => { checkedJobs.length === 6 ? send() : alert("עליך לבחור 6 משרות בלבד") }}><strong> הצג במסך הבית </strong></button>
            </div>
        </div>



    )


}