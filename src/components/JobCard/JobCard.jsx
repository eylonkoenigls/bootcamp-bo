import "./JobCard.css";

export default function JobCard({ allJobs, filteredJobs, setAllJobs, job_description, title, id, onHome }) {

  // console.log( job_description, title, id, onHome);
  const handlePress = (idd) => {
    console.log(idd);
    const newJob = allJobs.map(job => {
      if (idd === job._id) {
        console.log("yes");
        console.log(job.display_to_homePage);
        job.display_to_homePage = !job.display_to_homePage
        console.log(job.display_to_homePage);
      }
      return job
    }
    )
    console.log(newJob);
    setAllJobs(newJob)
  }



  // ((v) => {(v._id== idd) && v.display_to_homePage= !v.display_to_homePage})

  return (
    <div className="JobCard">
      <div className="JobCard-info">
        <label class="main-job-card">
          <input type="checkbox" value={id} onChange={(e) => handlePress(e.target.value)} checked={onHome} />
          {/* <span class="geekmark"></span> */}
        </label>

        <h5 className="company">{job_description}</h5>
        <h6 className="job-title"> תפקיד: {title} </h6>
      </div>
    </div>
  );
}




