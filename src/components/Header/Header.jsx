import { useContext } from "react";
import "./Header.css";
import { ReactComponent as LogoLs } from "../../assets/images/Logo@1.5x.svg";
import { Link } from "react-router-dom";
import AdminContext from "../../context/AdminContext";

function Header() {
  const { handleLogin, login } = useContext(AdminContext)
  return (
    <div className="header-container">
      <div className="div-header-start" >
        {login ? <Link className="div-login-button" onClick={() => { handleLogin(0) }} to="/login"> יציאה
        </Link> :
          <Link className="div-login-button" to="/login">הירשם  </Link>}
        {!login && <div className="div-no-account" >
          ?אין לך חשבון
        </div>}
      </div>

      <div>
        <div className="div-logo">
          <LogoLs />
        </div>
      </div>
    </div>
  );
}
export default Header;
