import React from "react";
import axios from 'axios'
import "./createNewPosition.css";
import { useState } from "react";
import { ReactComponent as CloseIcon } from "./Asset4@1.5x.svg"
export default function CreateNewPosition({ setCreate }) {
    const [setCreatePositions, setSetCreatePositions] = useState(false);
    const [title, setTitle] = useState();
    const [years_of_experience, setYears_of_experience] = useState();
    const [expired_date, setExpired_date] = useState();
    const [job_description, setJob_description] = useState();

    async function create_position(newPosition) {
        let res = await axios.post(
            `http://3.16.162.99/api/openPosition/newPosition`,
            newPosition
        );
        let data = await res.data;
        console.log(data);
    }
    const newPosition = {
        title,
        years_of_experience,
        expired_date,
        job_description,
    }


    return (
        <div className=".rectangle-main-pop-up-card-background">
            <div className="card-bg-main-pop-up-card-field">
                <div className="titleNewPosition">יצירת משרה חדשה</div>
                <div className="closeIcon-div-position">
                    <button onClick={() => setSetCreatePositions(false)} className="closeIcon" > <CloseIcon /> </button>
                </div>
                <div>
                    <form className="text-fields-pop-up-text">
                        <div className="rectangle-copy-10-three-first-input-fields">
                            <input
                                required
                                // value={title}
                                type="text"
                                className="all-input-field-styling"
                                placeholder="שם משרה"
                                onChange={(e) => setTitle(e.target.value)}
                            ></input>
                        </div>
                        <div className="rectangle-copy-10-three-first-input-fields">
                            <input
                                value={expired_date}
                                type="date"
                                className="all-input-field-styling"
                                placeholder="תאריך תפוגה"
                                onChange={(e) => setExpired_date(e.target.value)}
                                required
                            ></input>
                        </div>
                        <div className="rectangle-copy-10-three-first-input-fields">
                            <input
                                onChange={(e) => setYears_of_experience(e.target.value)}
                                value={years_of_experience}
                                type="text"
                                className="all-input-field-styling"
                                placeholder="שנות ניסיון"
                                required
                            ></input>
                        </div>
                        <div className="text-field-3-last-input-">
                            <div className="rectangle-copy-12-last-input-field">
                                <input
                                    value={job_description}
                                    type="text"
                                    className="all-input-field-styling"
                                    placeholder="תאור משרה"
                                    onChange={(e) => setJob_description(e.target.value)}
                                    required
                                >
                                </input>

                            </div>

                        </div>

                        <div className="send-pop-up-Form-buttons-new-position">
                            <div className="elements-text-text-icon-left-2">
                                <button type="submit"
                                    onClick={() => create_position(newPosition)} className="bg-pop-up-publish-button">פרסם</button>
                            </div>

                            <div className="elements-text-text-icon-right-">
                                <button onClick={() => setSetCreatePositions(false)} className="bg-pop-up-cancel-button">בטל</button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>

        </div>
    );
}


// import CreateNewPosition from '../createNewPosition/createNewPosition'
