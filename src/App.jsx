import "./App.css";
import api from "./axiosReq";
import AdminContext from "./context/AdminContext";
import { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route, Navigate, useNavigate } from "react-router-dom";
import CandidateList from "./components/candidateList/CandidateList";
import OpenPositions from "./components/openPositions/OpenPositions";
import FilledPositions from "./components/filledPositions/FilledPositions";
import PositionsOnHome from "./components/positionsOnHome/PositionsOnHome";
// import Navbar from "./components/navbar/Navbar";
import Login from "./components/login/Login";
import Header from "./components/Header/Header";
import { clear } from "@testing-library/user-event/dist/clear";
// const axios = require('axios');


function App() {

  const [loginDetails, setLoginDetails] = useState({});
  const [login, setLogin] = useState(Boolean(localStorage.LSaccessToken));

  const clear = () => {
    localStorage.clear("LSaccessToken");
    setLogin(Boolean(localStorage.LSaccessToken));
  }
  const handleLogin = async (event) => {
    if (event === 0) { return clear() }
    event.preventDefault();
    if (!localStorage.LSaccessToken) {
      const body = { username: event.target[0].value, password: event.target[1].value }
      const res = await api.post("/usermanager/manager/login", body);
      localStorage.LSaccessToken = res.data.accessToken;
      api.defaults.headers.common["Authorization"] = `bearer ${localStorage.LSaccessToken}`;
      setLogin(Boolean(localStorage.LSaccessToken))
    } else {
      clear()
    }
  }






  return (
    <>
      <div className="app">
        <AdminContext.Provider value={{ handleLogin, login }}>
          <Router>
            <Header />
            <div >
              {/* {login && <Navbar />} */}
              <Routes>
                <Route path="/" element={login ? <Navigate to="/OpenPositions" /> : <Navigate to="/login" />} />
                <Route path="/login" element={localStorage.LSaccessToken ? <Navigate to="/OpenPositions" /> : <Login />} />
                <Route path="/OpenPositions" element={<OpenPositions />} />
                <Route path="/FilledPositions" element={<FilledPositions />} />
                <Route path="/CandidateList" element={<CandidateList />} />
                <Route path="/PositionsOnHome" element={<PositionsOnHome />} />
              </Routes>
            </div>
          </Router>
        </AdminContext.Provider>
      </div>
    </>
  )
}

export default App;
